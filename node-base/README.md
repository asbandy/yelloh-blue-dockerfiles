# Example docker run for ashawnbandy/node-base

docker run -ti --rm --network=host -v /private:/private -v /home/developer:/home/developer -v /var/run/docker.sock:/var/run/docker.sock --name node-base ashawnbandy/node-base